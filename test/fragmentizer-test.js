/* eslint-env mocha */
'use strict'

const { expect, outdent } = require('./harness')
const extension = require('antora-fragmentizer-extension')

describe('fragmentizer listener', () => {
  let listener

  beforeEach(() => {
    extension.register.call({ once: (_, fn) => (listener = fn) })
  })

  it('should not fragmentize HTML if page-fragmentize attribute is not set', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div id="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-layout': 'home' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents).to.eql(pages[0].contents)
    expect(Object.keys(contents).length).to.eql(contents.length)
  })

  it('should not fragmentize HTML if asciidoc property is not set', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div id="bar">bar</div>
        `),
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents).to.eql(pages[0].contents)
    expect(Object.keys(contents).length).to.eql(contents.length)
  })

  it('should not fragmentize HTML if out property is not set', () => {
    const pages = [
      {
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div id="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents).to.eql(pages[0].contents)
    expect(Object.keys(contents).length).to.eql(contents.length)
  })

  it('should fragmentize HTML by ID by default', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div id="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('foo', '<div id="foo">foo</div>')
    expect(contents).to.have.own.property('bar', '<div id="bar">bar</div>')
  })

  it('should select parent section for heading when fragmentizing HTML by ID', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div class="sect1">
          <h2 id="section-a">Section A</h2>
          <div class="sectionbody">
          <div class="paragraph">
          <p>content</p>
          </div>
          </div>
          </div>
          <div class="sect1">
          <h2>Section B</h2>
          <div class="sectionbody">
          <div class="sect2">
          <h3 id="nested-section">Nested Section</h3>
          <div class="paragraph">
          <p>content</p>
          </div>
          </div>
          </div>
          </div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('section-a')
    expect(contents['section-a']).to.match(/^<div class="sect1">/)
    expect(contents).to.have.own.property('nested-section')
    expect(contents['nested-section']).to.match(/^<div class="sect2">/)
  })

  it('should not select parent for discrete heading when fragmentizing HTML by ID', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div class="sect1">
          <h2>Section</h2>
          <div class="sectionbody">
          <div class="openblock">
          <div class="content">
          <h3 id="heading-a" class="discrete">Heading A</h3>
          <div class="paragraph">
          <p>content</p>
          </div>
          </div>
          </div>
          <div class="sect2">
          <h3>Nested Section</h3>
          <h3 id="heading-b" class="discrete">Heading B</h3>
          <div class="paragraph">
          <p>content</p>
          </div>
          </div>
          </div>
          </div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('heading-a', '<h3 id="heading-a" class="discrete">Heading A</h3>')
    expect(contents).to.have.own.property('heading-b', '<h3 id="heading-b" class="discrete">Heading B</h3>')
  })

  it('should fragmentize HTML by ID with custom selector', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div id="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': 'div by id' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('foo', '<div id="foo">foo</div>')
    expect(contents).to.have.own.property('bar', '<div id="bar">bar</div>')
  })

  it('should fragmentize HTML by singular selector', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div class="foo">foo</div>
          <div class="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '.bar as bar' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('bar', '<div class="bar">bar</div>')
  })

  it('should set property of alias to empty string if selector does not match', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div class="foo">foo</div>
          <div class="bar">bar</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '.baz as baz' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('baz', '')
  })

  it('should fragmentize HTML by collection selector', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div class="tile">A</div>
          <div class="tile">B</div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '.tile as tiles[]' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.deep.property('tiles', ['<div class="tile">A</div>', '<div class="tile">B</div>'])
  })

  it('should fragmentize HTML by collection selector with multiple selectors', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <section><p>A</p></section>
          <div><ul><li>B</li></ul></div>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': 'section, div ul as elements[]' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.deep.property('elements', ['<section><p>A</p></section>', '<ul><li>B</li></ul>'])
  })

  it('should fragmentize HTML using all queries', () => {
    const pages = [
      {
        out: {},
        contents: Buffer.from(outdent`
          <div id="foo">foo</div>
          <div class="unique">unique</div>
          <ul>
          <li>one</li>
          <li>two</li>
          </ul>
        `),
        asciidoc: {
          attributes: { 'page-fragmentize': '[id] by id; .unique as unique; li as items[]' },
        },
      },
    ]
    listener({ contentCatalog: { getPages: (predicate) => pages.filter(predicate) } })
    const contents = pages[0].contents
    expect(contents).to.be.instanceOf(Buffer)
    expect(contents.toString()).to.be.empty()
    expect(contents).to.have.own.property('foo', '<div id="foo">foo</div>')
    expect(contents).to.have.own.property('unique', '<div class="unique">unique</div>')
    expect(contents).to.have.own.deep.property('items', ['<li>one</li>', '<li>two</li>'])
  })
})
