'use strict'

const { parse } = require('node-html-parser')

const HEADING_TAGS = { h2: true, h3: true, h4: true, h5: true, h6: true }

module.exports.register = function () {
  this.once('documentsConverted', ({ contentCatalog }) => {
    contentCatalog.getPages((page) => {
      const queries = page.out ? ((page.asciidoc || {}).attributes || {})['page-fragmentize'] : undefined
      if (queries != null) page.contents = fragmentize(page.contents, queries)
    })
  })
}

function fragmentize (contents, queries) {
  const root = parse('<div>' + contents.toString() + '</div>')
  contents = {}
  ;(queries ? queries.split('; ') : ['[id] by id']).forEach((query) => {
    if (query.endsWith(' by id')) {
      root.querySelectorAll(query.substr(0, query.length - 6)).forEach((el) => {
        const { id, parentNode, rawTagName } = el
        if (
          HEADING_TAGS[rawTagName] &&
          parentNode &&
          !el.classList.contains('discrete') &&
          parentNode.classList.contains('sect' + String.fromCharCode(rawTagName.charCodeAt(1) - 1))
        ) {
          el = parentNode
        }
        if (id) contents[id] = el.outerHTML
      })
    } else if (query.endsWith('[]')) {
      const asIdx = query.lastIndexOf(' as ')
      const selector = query.substr(0, asIdx)
      const alias = query.substr(asIdx + 4)
      contents[alias.substr(0, alias.length - 2)] = root.querySelectorAll(selector).map((el) => el.outerHTML)
    } else {
      const asIdx = query.lastIndexOf(' as ')
      const selector = query.substr(0, asIdx)
      const alias = query.substr(asIdx + 4)
      contents[alias] = (root.querySelector(selector) || { outerHTML: '' }).outerHTML
    }
  })
  return Object.assign(Buffer.alloc(0), contents)
}
